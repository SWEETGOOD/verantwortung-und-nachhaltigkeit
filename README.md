![](logo.png)

# 🌱 Verantwortung & Nachhaltigkeit
> Transparenz- und Nachhaltigkeitsbericht der Firma SWEETGOOD – revisionssicher, ehrlich und authentisch

<br />

Der Bericht ist im Moment **ausschließlich in deutscher Sprache** verfügbar:

# [🔗 Transparenz- und Nachhaltigkeitsbericht](transparenz-und-nachhaltigkeitsbericht.md)

<br />

[📄 Einzelne Versionen anzeigen](https://codeberg.org/SWEETGOOD/verantwortung-und-nachhaltigkeit/tags)

[📜 Alle Änderungen anzeigen](https://codeberg.org/SWEETGOOD/verantwortung-und-nachhaltigkeit/commits/branch/main)

<hr />

Weitere **Hintergrundinformationen** zum Transparenz- und Nachhaltigkeitsbericht findest du auf der folgenden Internetseite:  
[🔗 SWEETGOOD | Verantwortung & Nachhaltigkeit](https://sweetgood.de/verantwortung-und-nachhaltigkeit)