![](logo.png)

# Transparenz- und Nachhaltigkeitsbericht
| Version (semver) | **v1.3.1** |
| --- | --- |
| Zuletzt geändert | **Christian Süßenguth @ 03.03.2025** |

---

## Inhaltsverzeichnis
- [Grundsätzliches und Zwischenmenschliches](#grundsätzliches-und-zwischenmenschliches)
  - [Reparieren statt neu kaufen](#reparieren-statt-neu-kaufen)
- [Lieferant:innen (Einkauf)](#lieferant-innen-einkauf)
  - [Büromaterial](#büromaterial)
  - [IT-Equipment](#it-equipment)
  - [Smartphone](#smartphone)
- [Mitarbeitende (Arbeitsbedingungen und Bezahlung)](#mitarbeitende-arbeitsbedingungen-und-bezahlung)
- [Soziales Engagement](#soziales-engagement)
- [Empfehlungen und Provisionen](#empfehlungen-und-provisionen)
- [Finanzen](#finanzen)
	- [Zahlungsdienstleister](#zahlungsdienstleister)
	- [Steuervermeidung](#steuervermeidung)
	- [Ehemalige Geschäftsbeziehungen](#ehemalige-geschäftsbeziehungen)
- [Energieversorgung](#energieversorgung)
	- [Strom](#strom)
	- [Wärme](#wärme)
	- [Energieeffizienz](#energieeffizienz)
	- [Ehemalige Energieversorger](#ehemalige-energieversorger)
- [Hosting](#hosting)
	- [Internetanbindung](#internetanbindung)
	- [Mobilfunk & Telefonie](#mobilfunk-telefonie)
	- [Videokonferenzen](#videokonferenzen)
- [Mobilität](#mobilität)
- [Pioniergeist](#pioniergeist)
- [Persönlicher Einkaufskorb (Lebens- und Reinigungsmittel)](#persönlicher-einkaufskorb-lebens-und-reinigungsmittel)
  - [CO2-Einsparung](#co2-einsparung)

<br />

<hr />

## Grundsätzliches und Zwischenmenschliches
- Meine Kund:innen haben **stets die volle Kontrolle** über ihre gesamte IT-Infrastruktur
- Ich schaffe **keine künstlichen Abhängigkeiten** von mir als Dienstleister
- **Transparenz** und **Offenheit** sind meine Grundhaltungen
- Keine Empfehlung von **unnötigen Produkten / Diensten**, ich erhalte **keine Provisionen**
- Einmal im Monat versende ich einen **kostenfreien IT-Zustandsbericht** ([SWEETreport](https://www.sweetreport.de))
- Sämtliche (personenbezogenen) Daten kommen bei mir **nicht** mit unfreier Software von [Konzernen wie Microsoft, Alphabet (Google), Apple, Amazon oder Meta (Facebook)](https://de.wikipedia.org/wiki/Big_Tech) in Berührung

<br />

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

**Alle Zugangsdaten, Firmendaten und Dokumentationen** liegen **grundsätzlich bei der Kund:in** und nicht bei mir als Dienstleister oder in der "Cloud". Damit stärke ich die **Unabhängigkeit der Kund:in** von mir als Dienstleister und schaffe **keine künstlichen Abhängigkeiten**.  
Wer mit mir zusammenarbeiten möchte, soll das aufgrund meiner **Persönlichkeit** und meiner **Leistungen** tun und nicht, weil er / sie durch Verträge oder Abhängigkeiten dazu gezwungen ist.

Alle Kosten und durchzuführenden Maßnahmen werden **transparent** und **offen** kommuniziert. Meine Preise stehen [auf meiner Homepage](https://sweetgood.de/costa-quanta) und **erhöhen sich** für langjährige Bestandskund:innen **nur in absoluten Ausnahmefällen** und mit Vorlaufzeit.

Die letzte Preisanpassung für Bestandskund:innen erfolgte am 15. Februar 2023. Der Stundensatz wurde dabei um die Inflationsrate von 5,88% angehoben, um die durch die Inflation gestiegenen Preise auszugleichen.

Ich empfehle meinen Kund:innen ausschließlich Produkte, die sie auch **wirklich** benötigen. Ich erhalte **keine Provisionen für Produktempfehlungen**. Dadurch bin ich **unabhängig** und nicht darauf angewiesen, bestimmte Vertriebsziele zu erreichen.

Alle Tätigkeiten werden **detailliert** und **transparent** in einer Tätigkeitsbeschreibung aufgeschlüsselt und zusätzlich zur Rechnung versendet. So ist jederzeit ersichtlich, welche Leistungen meinerseits erbracht wurden.

Meine Kund:innen erhalten einmal im Monat einen **kostenfreien Zustandsbericht** ihrer Unternehmens-IT – den sogenannten [**SWEETreport**](https://www.sweetreport.de)**. Mit Hilfe des Berichtes sind sie jederzeit bestens informiert, wie es um die Gesundheit und Sicherheit der eigenen IT bestellt ist.

Jede Kund:in ist nur solange König:in, solange sie auch **zu mir und meinen Werten passt**. Ich habe in der Vergangenheit bereits **Geschäftsbeziehungen beendet**, weil bspw. der Umgang der Kund:in mit den eigenen Mitarbeiter:innen nicht mit meinen Werten vereinbar war. Auch betreue ich grundsätzlich keine Unternehmen aus Branchen, die im Widerspruch zu meinen Werten stehen (z.B. Unternehmen, die die Umwelt ausbeuten, Waffen herstellen, usw.).

Da ich in meiner Firma fast ausschließlich **Open Source-Software** einsetze, kommen sämtliche (personenbezogenen) Daten **nicht** mit unfreier Software von [Konzernen wie Microsoft, Alphabet (Google), Apple, Amazon oder Meta (Facebook)](https://de.wikipedia.org/wiki/Big_Tech) in Berührung.

</details>

<br />

### Reparieren statt neu kaufen
Grundsätzlich **repariere ich Geräte** bzw. rüste sie auf, bevor ich meinen Kund:innen etwas Neues verkaufe (bzw. Neuware heraussuche). Dadurch nutze ich bereits vorhandene Ressourcen **effektiv** und schone den **Geldbeutel** meiner Kund:innen.

Durch den **Einsatz von Linux** erhöhe ich außerdem die Lebenszeit von Geräten **teils um viele Jahre**, zu denen sie vom Hersteller schon lange keine offiziellen Updates mehr erhalten würden.

<br />

<hr />

## Lieferant:innen (Einkauf)
- Ich verkaufe Ware **nur in Ausnahmefällen**. Stattdessen suche ich passende Ware heraus und die Kund:in **bestellt sie selbst**
- Ich bestelle grundsätzlich **nicht bei Amazon**
- Ich empfehle **gebrauchte, wiederaufbereitete Geräte** statt Neuware

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Hardware und sonstige Ware **verkaufe ich nur in Ausnahmefällen**. Stattdessen suche ich für meine Kund:innen das **passendste** (und ggf. **günstigste**) Angebot heraus und die **Kund:in bestellt es selbst**. Dadurch profitieren meine Kund:innen vom **niedrigen Einkaufspreis** und ich brauche **keine Margen** einzukalkulieren, die das Produkt künstlich verteuern würden. Dadurch fällt **lediglich die Arbeitszeit** für die Produktrecherche gemäß meinem Stundensatz an.

Als Plattformen für die Produktrecherche nutze ich [gh.de](https://gh.de) und [ebay.de](https://www.ebay.de). Ich pflege außerdem regelmäßige Geschäftsbeziehungen zu [reichelt.de](https://www.reichelt.de) und [memo.de](https://www.memo.de). Die Geschäftsbeziehung zu Büromarkt Böttcher AG habe ich im Februar 2025 mit sofortiger Wirkung **beendet**, als [bekannt wurde](https://www.tagesschau.de/inland/regional/thueringen/boettcher-aufsichtsrat-afd-spende-100.html), dass hier wohl eine starke Sympathie zur AfD besteht.

Bei gebrauchten Business-Geräten (sog. refurbished) greife ich auf [afbshop.de](https://www.afbshop.de) und [greenpanda.de](https://www.greenpanda.de) zurück.

Ich bestelle **grundsätzlich nicht bei Amazon**.

**Neuware** beziehe ich nur in Ausnahmefällen, denn gebrauchte Business-Hardware ist bei gleicher Qualität erheblich **günstiger** und zudem **nachhaltiger** (vergleichbar mit Jahreswägen).

</details>

<br />

### Büromaterial
- **Drucker und Kopierpapier** sind mit dem **Blauen Engel** zertifiziert
- Als **Paketklebeband** verwende ich **Kraftpapier auf Natronbasis** ohne Kunststoff
- **Versandverpackungen** werden gesammelt und **wiederverwendet**
- **Briefkuverts** haben ein **biologisch abbaubares Sichtfenster** aus Maisstärke
- Rechnungen / Informationen werden **per E-Mail oder Messenger** statt per Post verschickt

<br />

### IT-Equipment
- Ich selbst nutze ebenfalls **gebrauchte, refurbished Hardware** und kaufe Hardware nur in Ausnahmefällen neu
- **Ausrangierte Geräte** werden entweder **gespendet** (z.B. an die [Computertruhe](https://computertruhe.de)) oder **anderweitig genutzt bzw. weiterverkauft**

<br />

### Smartphone
- Ich selbst verwende nur **nachhaltige** oder **gebrauchte Android-Geräte**
- **Keine Geräte von Apple**
- Ausschließlich **Google-freies Android**

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Als Smartphone kommen bei mir nur **nachhaltige** oder **gebrauchte** Geräte mit **vollständig googlefreiem Android** zum Einsatz. Google ist für mich **keine vertrauenswürdige Firma**, da die sog. Google-Play-Services volle Systemrechte haben und auf einem Smartphone schalten und walten können, wie sie wollen. **Jedes** US-amerikanische Unternehmen muss sich außerdem an den [CLOUD ACT](https://de.wikipedia.org/wiki/CLOUD_Act) bzw. [PATRIOT ACT](https://de.wikipedia.org/wiki/USA_PATRIOT_Act) halten, weswegen Softwareanbieter aus den USA für mich **grundsätzlich** als **nicht vertrauenswürdig** eingestuft werden, solange ich den Quelltext der Software nicht einsehen und sie selbst betreiben kann.

**Apple-Geräte** sind für mich wegen der **schlechten Reparierbarkeit**, des **nicht quelloffenen Betriebssystems**, dem **Accountzwang** und den **prekären Herstellungsbedingungen** ein **No-Go**.

#### Meine Smartphone-Historie
- 2013 - 2015: **Fairphone 1** (neu), 1x Akku getauscht
- 2015 - 2017: **Fairphone 2** (gebraucht), 1x Akku/Display/Bottom-Modul getauscht
- 2016: **SHIFT7+** (habe es nach kurzer Zeit wg. div. Defekte zurückgegeben)
- 2017 - 2021: **OnePlus 3** (gebraucht), 2x Akku getauscht
- 2021 - September 2023: **OnePlus 5** (gebraucht), 1x Akku getauscht
- September 2023 - Februar 2024: Temoräre Nutzung des **OnePlus 3**, da das **OnePlus 5** verloren ging
- seit Februar 2024: **Fairphone 5** (neu)

</details>

<br />

<hr />

## Mitarbeitende (Arbeitsbedingungen und Bezahlung)
- **Mindestlohn** für Mitarbeitende: **15,00 € / Std.**
- **Freie Einteilung** der Arbeitszeit
- **Kein** "Druck von oben"
- Unterstützung bei der **Persönlichkeitsentwicklung**

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Der **Mindestlohn für Mitarbeitende** in der Firma SWEETGOOD beträgt **15,00 €** und liegt damit ca. **17%** über dem gesetzlichen Mindestlohn von 2025.

Die Mitarbeitenden können ihre **Arbeitszeit frei bestimmen** und bekommen auf Wunsch ein **passendes Gerät** für die Arbeit und die dafür notwendige Infrastruktur kostenfrei gestellt.

Bei Fragen stehe ich **jederzeit tatkräftig** zur Verfügung.

Es gibt **keinen Druck von oben**, der die Mitarbeitenden zu mehr / besserer Arbeitsleistung zwingt.

Stattdessen bin ich davon überzeugt, dass Mitarbeitende in einem **angenehmen Arbeitsumfeld** von selbst **zur Höchstform auflaufen**, wenn ihnen die Ausübung ihres Berufs **Spaß und Freude** bereitet. Andernfalls ist die Person vmtl. noch nicht am richtigen Platz.

Ich versuche dieses angenehme Arbeitsumfeld **zu schaffen** und unterstütze die Mitarbeitenden im Rahmen meiner Möglichkeiten bei der **Persönlichkeitsentwicklung** – natürlich nur, wenn diese auch daran interessiert sind.

</details>

<br />

<hr />

## Soziales Engagement
Ich **spende regelmäßig** für verschiedene **soziale und politische Initiativen** und freie **Open Source-Software**.

Eine **detaillierte Übersicht** findest du [im **andersGOOD Blog**](https://andersgood.de/warum-ich-regelmaessig-spende#ich-selbst-spende-fuer).

<br />

<hr />

## Empfehlungen und Provisionen
Ich empfehle meinen Kund:innen ausschließlich Lösungen, die ich entweder **selbst getestet** habe oder **im eigenen Unternehmen einsetze**. Eine Übersicht aller Tools, die ich empfehle, findest du auf meiner Homepage unter [**Transparenz**](https://sweetgood.de/transparenz).

Ich erhalte **keine Provisionen** für meine Empfehlungen und erhalte mir durch diese Entscheidung meine **Unabhängigkeit**.

**Einzige Ausnahme** bilden jährliche Provisionszahlungen der Firma **netcup**, welche noch **aus früheren Jahren** existieren. Dabei wurden mit Hilfe von Affiliate-Coupons Webhosting-Tarife für Kund:innen bestellt, von deren jährlichen Kosten ich **10% Provision** erhalte. Für die Kund:innen entstanden daraus keine Nachteile.
Die **jährliche Provisions-Summe** beträgt rund 100-150 €.

Bei den folgenden Produkten liegt mein **Einkaufspreis unterhalb des Verkaufspreises für Kund:innen**:
- IKARUS anti.virus
- ~grommunio~ (biete ich seit 2024 nicht mehr aktiv an)

<br />

<hr />

## Finanzen
Das Firmenkonto liegt bereits **seit 2014** bei der [**nachhaltigen Öko-Bank GLS**](https://www.gls.de/). Seit 2023 habe ich außerdem ein Prepaid-Kreditkarten-Konto bei der [**Tomorrow Bank**](https://www.tomorrow.one/de-DE/) und seit 2025 **keine PayPal-Konten** mehr.

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

**Kriterien** für die Auswahl eines für mich in Frage kommenden Kreditinstituts sind:
- 🌿 Nachhaltige und ökologische Ausrichtung
- ☢️ Keine Investitionen in die Kohle- / Atomenergie-Branche
- 🛢 Keine Geschäfte mit Unternehmen, die der Umwelt schaden
- 🤑 Keine Spekulationen im Nahrungsmittel-, Saatgut- und Rohstoffbereich

</details>

<br />

### Zahlungsdienstleister
- Ich nutze – wo immer möglich – **(Echtzeit-)Überweisungen**
- **Klarna**/**SOFORT** nutze ich aus Prinzip nicht, **PayPal** habe ich zu Februar 2025 vollständig aus dem Unternehmen verbannt und nutze für Onlinekäufe die [**girocard Debit Mastercard**](https://www.gls.de/konten-karten/karten/bankcard/) der GLS Bank

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Seit Februar 2025 nutze ich **kein PayPal** mehr und die zugehörigen Accounts sind stillgelegt. Das gilt auch für den Privatbereich. **Klarna nutze ich aus Prinzip nicht**. Durch die Nutzung des Online-Marktplatzes eBay war es nicht einfach, zu PayPal und Klarna/SOFORT eine Alternative zu finden. Ich hatte hier also sprichwörtlich die Wahl zwischen Pest und Cholera.

Zum Glück bietet die GLS Bank seit einiger Zeit die sog. [**girocard Debit Mastercard**](https://www.gls.de/konten-karten/karten/bankcard/) an. Das ist eine (plastikfreie) Holz-EC-Karte, die gleichzeitig auch eine Debit-**Kreditkarte** beinhaltet. Der Vorteil einer Debit-Kreditkarte ist, dass sie – wie PayPal – die gleiche Funktion erfüllt und Onlinezahlungen immer direkt vom Konto abgebucht werden. Es gibt also **keinen klassischen Kredit**, der die SCHUFA beeinflussen würde. Die Akzeptanz von MasterCard bei Onlinezahlungen ist ebenfalls **sehr hoch**, so dass mir dadurch keinerlei Nachteile entstehen. Im Gegenteil, ch gewinne sogar etwas, ich habe nämlich einen **persönlichen, Deutsch sprechenden Ansprechpartner** bei der GLS Bank. Bei PayPal hatte ich nur mit (nutzlosen) Chatbots das Vergnügen.

Da **giropay / paydirekt** leider [Ende 2024 eingestellt wurde](https://archive.ph/KuUGH) und die datenschutzfreundliche europäische Alternative namens [Wero](https://wero-wallet.eu/de/) bei der GLS Bank noch auf sich warten lässt, ist die Debit Kreditkarte eine **sehr gute Überbrückungslösung**, um PayPal endlich aus meinem ~Unternehmen~ Leben verbannen zu können.

</details>

<br />

### Steuervermeidung
Ich besitze **keine Offshore-Konten** und **praktiziere keine Steuervermeidungs-Strategien** durch Verlagerung meiner Gewinne in steuerbegünstigte Länder.

<br />

### Ehemalige Geschäftsbeziehungen
<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

In der Vergangenheit war ich mit einer **Prepaid-Kreditkarte** Kunde bei
- **Wirecard AG** – seit 2020 insolvent
- **NetBank** – seit 2022 insolvent
- **Bitwala / Nuri** - seit 2022 insolvent
- **Revolut** - seit 2022 gekündigt, da App-Zwang

</details>

<br />

<hr />

## Energieversorgung

### Strom
Sämtliche Standorte werden **ausschließlich** mit **Strom aus regenerativen Quellen** betrieben – und das **seit Firmengründung** im Jahre 2016 (genau genommen seit 2013). Seit **2021** beziehe ich nur noch [**echten Ökostrom**](https://www.oekostrom-anbieter.info/oekostrom/echter-oekostrom).

Ende Juni 2024 ging außerdem eine [**Balkonsolaranlage mit 1,76 KWp**](https://www.marktstammdatenregister.de/MaStR/Einheit/Detail/IndexOeffentlich/8350219) und dazugehörigem [**Batteriespeicher mit 1600 Wh**](https://www.marktstammdatenregister.de/MaStR/Einheit/Detail/IndexOeffentlich/8350220) am Standort Pleinfeld ans Netz. Damit wird die IT-Infrastruktur der Firma SWEETGOOD bei guten Wetterbedingungen **ausschließlich von der Sonne gespeist** und quasi autark betrieben.

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

#### 🏢 Pleinfeld (Hauptsitz)
**EWS Schönau** ([📎 EWS ÖKOSTROM](belege/energieversorgung/ews-schoenau-strom-pleinfeld.png))

| Jahr | Gesamt | Serverinfrastruktur | davon Klimatisierung |
| --- | --- | --- | --- |
| 2021 | ~ 2.000 kWh (seit Mai) | ~ 1.900 kWh (seit Juni) | nicht erfasst |
| 2022 | ~ 3.300 kWh | ~ 3.100 kWh | nicht erfasst |
| 2023 | ~ 4.300 kWh | ~ 3.900 kWh | ~ 900 kWh (seit Mai) |
| 2024 | ~ 3.900 kWh | ~ 3.700 kWh | ~ 600 kWh |

**Balkonsolaranlage**
- [Balkonsolaranlage mit 1,76 KWp](https://www.marktstammdatenregister.de/MaStR/Einheit/Detail/IndexOeffentlich/8350219)
- [Batteriespeicher mit 1600 Wh](https://www.marktstammdatenregister.de/MaStR/Einheit/Detail/IndexOeffentlich/8350220)

Nutzung von Solarstrom von Ende Juni 2024 bis Anfang Februar 2025: **~ 850 kWh**

Tagesproduktion je nach Wetterbedingungen zwischen **0,5 - 8 kWh**

</details>

<br />

### Wärme
Da die **Wärmeversorgung** bislang über die Vermieter:innen läuft, kann ich hier leider **keinen Einfluss** auf die Wahl eines ökologische(re)n Tarifes nehmen.

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

#### 🏢 Pleinfeld (Hauptsitz)
**Gas**-Heizung (Abrechnung über Nebenkosten)
- Verbrauch 2021: *nicht relevant / größtenteils Abwärme aus Serverbetrieb*
- Verbrauch 2022: *nicht relevant / größtenteils Abwärme aus Serverbetrieb*
- Verbrauch 2023: *nicht relevant / größtenteils Abwärme aus Serverbetrieb*
- Verbrauch 2024: *nicht relevant / größtenteils Abwärme aus Serverbetrieb*

</details>

<br />

### Energieeffizienz
- Grundsätzlich überall **LED-Beleuchtung**
- Serverinfrastruktur besteht aus **gebrauchter, energieeffizienter Hardware**
- Bei guten Wetterbedingungen wird die IT-Infrastruktur **zu 100%** von der Balkonsolaranlage versorgt
- **Linux** als Betriebssystem **spart Energie** und ermöglicht **längere Nutzung alter Hardware**

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Ich achte an allen Standorten auf **energiesparende LED-Beleuchtung**. Die Beleuchtung wird entweder per Bewegungsmelder gesteuert oder bewusst abgeschaltet, wenn diese nicht benötigt wird.

Die **Serverinfrastruktur** besteht größtenteils aus **gebrauchter Hardware**, die entsprechend **energieeffizient** arbeitet. Geräte und virtuelle Maschinen, die längere Zeit nicht benötigt werden, werden heruntergefahren, um Ressourcen zu sparen.

Durch den Einbau zweier Lüfter in den Serverschrank Anfang 2022 konnte die Laufzeit der **Klimaanlage** und damit deren **Stromverbrauch signifikant reduziert** werden. Das wurde durch Ergänzen von **zwei weiteren Lüftern** in 2024 noch zusätzlich verbessert.

Die **Balkonsolaranlage** am Standort in Pleinfeld versorgt **die gesamte Infrastuktur** bei guten Wetterbedingungen **zu 100% mit Strom**.

Auf **allen produktiven Systemen läuft Linux** oder ein entsprechender Ableger (sog. Derivat). Der Stromverbrauch von Linux ist im Vergleich zu Windows **signifikant niedriger**, da Updates effizient ausgeführt werden und **keine unnötigen Telemetrie-Daten** an den Softwarehersteller übermittelt werden. Auch gibt es bei Linux ([anders als mit der Einführung von Windows 11](https://www.golem.de/news/windows-11-geplante-obsoleszenz-ist-schlecht-microsoft-2109-159574.html)) keine "Mindestanforderungen", die Hardware von heute auf morgen in Elektroschrott verwandelt.

</details>

<br />

### Ehemalige Energieversorger

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

#### Pleinfeld
Strom: **Bürgerwerke eG** (04/2021 bis 02/2025)  
([📎 ECHTER Ökostrom-Tarif](belege/energieversorgung/buergerwerke-strom-pleinfeld.png))

#### Traunstein
Strom: **Stadtwerke Traunstein**  
([📎 Wasserkraft-Tarif](belege/energieversorgung/swt-strom-traunstein.png))

Heizung: **Hackschnitzel** (Abrechnung über Nebenkostenpauschale)

#### Weißenburg
Strom: **MONTANA Energie**  
[📎 Tarif STROM garant 12](belege/energieversorgung/montana-strom-weissenburg.png)

Gas: **MONTANA Energie**  
[📎 Tarif GAS garant 12](belege/energieversorgung/montana-gas-weissenburg.png)

#### Spalt
Strom: **ESWE Versorgung AG**  
[📎 Tarif Natur STROM](belege/energieversorgung/eswe-strom-spalt.png)

</details>

<br />

<hr />

## Hosting
- Ich hoste meine Server primär **am Firmenstandort in Pleinfeld** und zusätzlich bei **netcup** und **HETZNER Online**
- **100% Ökostrom** ist **absolute Pflichtvoraussetzung** für die Auswahl des Hosters
  - Eine **ausführliche Analyse der in Frage kommenden Hostingunternehmen und meiner Erfahrungen** findest du [auf meiner Homepage unter **Transparenz**](https://sweetgood.de/transparenz#webhosting)
- Die relevante Infrastruktur (aktuell **> 30 Server / VMs**) betreibe ich **selbst** und habe damit vom Strom bis zur verwendeten Hardware und der Energieeffizienz alles selbst in der Hand

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Beim Hosting von Servern habe ich **Ende 2024** einen **großen Schritt gewagt** und **alle Produktivsysteme** an den **Firmenstandort in Pleinfeld** geholt. Damit habe ich vom [echten Öko-Strom](https://www.oekostrom-anbieter.info/oekostrom/echter-oekostrom) bis zur verwendeten Hardware und der Energieeffizienz alles selbst in der Hand.

Seit 2015 habe ich außerdem Server bei der Firma [**netcup**](https://www.netcup.de/), die ihrerseits wieder Server bei HETZNER Online stehen haben. Auch wenn ich dieses Unternehmen nicht als sonderlich nachhaltig bezeichnen würde, so setzt es nach [eigenen Angaben](https://www.netcup.de/ueber-netcup/oekostrom.php) auf **Ökostrom**. **Nachprüfbar** ist das für mich als Kunde **leider nicht**, weswegen ich dort nur noch **zwei Server** laufen habe.

> **netcup GmbH** gehört seit 2018 zu [anexia](https://www.anexia.com/), weitere Beteiligungen stehen bei [NorthData](https://www.northdata.de/netcup+GmbH,+Karlsruhe/Amtsgericht+Mannheim+HRB+705547).

Bei [**HETZNER**](https://www.hetzner.com/de/) habe ich eine sog. [**Storage Box**](https://www.hetzner.com/de/storage/storage-box/) für das Ablegen von Backups gebucht und seit Anfang 2025 zusätzlich eine [**winzige Cloud-VM**](https://www.hetzner.com/de/cloud/), die mir zusätzliche IPv4-Adressen für den Firmenstandort bereitstellt. Meine Einschätzung zu HETZNER als **sehr nachhaltigem Hostingunternehmen** findest du [auf meiner Homepage unter **Transparenz**](https://sweetgood.de/transparenz#webhosting).

> Die **HETZNER Online GmbH** kann nach [NorthData](https://www.northdata.de/Hetzner+Online+GmbH,+Gunzenhausen/Amtsgericht+Ansbach+HRB+6089) als "inhabergeführt" bezeichnet werden.

Aktuell betreibe ich bei:
- netcup: **zwei Server**
- HETZNER: **eine Storage Box** und **eine winzige Cloud-VM**
- Am Firmenstandort: **> 30 Server / VMs**

#### Ehemalige Hoster
<details>
<summary>➡️ <strong>KeyWeb</strong></summary>
<br />
2022 bin ich mit dem Hauptserver zu [**KeyWeb**](https://www.keyweb.de/de/) umgezogen, da ich mich schon länger nach einem **alternativen Hoster** zu netcup umsah. Das Unternehmen zeichnet sich durch ein [hohes soziales und ökologisches Engagement](https://www.keyweb.de/de/keyweb/keyeco) aus. Auch ist dort [nachprüfbar](https://assets.keyweb.de/uploads/files/pdf/Zertifikat_Oekostrom_Wasserkraft_2022.pdf), aus welcher Quelle der Strom stammt. Allerdings sind dadurch die monatlichen Kosten für den Server um rund ⅓ gestiegen.

**Ende 2024** habe ich meinen Server bei KeyWeb wieder **gekündigt**, weil ich mit **der Qualität** des vServer-Hostings und mit dem **Support** mehr als **unzufrieden** war.

> An der **KeyWeb AG** sind gemäß Auskunft von [NorthData](https://www.northdata.de/Keyweb+AG,+Erfurt/Amtsgericht+Jena+HRB+112403) seit 2022/2023 das Energieunternehmen TEAG Thüringer Energie AG (der Ökostrom-Energieversorger) und die KEBT Kommunale Energie Beteiligungsgesellschaft zu je 25,1 % beteiligt.

</details>

</details>

<br />

### Internetanbindung
- Ich bin Kunde von **Vodafone** und **o2**
- Leider gibt es **keine in Frage kommenden nachhaltigen Provider**

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Der Hauptstandort ist redundant mit **zwei Internetanschlüssen** angebunden. Einer davon ist ein Kabelanschluss von **Vodafone**, der zweite ein VDSL-Anschluss von **o2**. Zur Anbindung für den Notfall gibt es noch einen Prepaid-LTE-Vertrag von **o2**.

Sehr gerne würde ich hier auf **nachhaltige Provider** setzen, allerdings gibt es bislang **keine geeigneten Alternativen**.

</details>

<br />

### Mobilfunk & Telefonie
- Ich bin langjähriger Kunde der **Telekom**
- Leider gibt es **keine in Frage kommenden nachhaltigen Provider**
- Ich trage **seit 2014 mein Smartphone ausschließlich im Flugmodus** bei mir, daher ist die **klassische Telefonie** als Form der Kommunikation für mich weitestgehend irrelevant
- Für **verschlüsselte Festnetz-Telefonie** nutze ich den VoIP-Provider [**inopla / ComDesk**](https://www.inopla.de/)
- Primär nutze ich allerdings **Ende-zu-Ende-verschlüsselte Verbindungen** über Messenger wie **Telegram** oder **Signal**

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Hier bin ich seit Jahren Kunde der **Telekom**. Nachdem 2016 eine Portierung der Mobilfunknummer zu o2 gehörig schiefgegangen ist, kommt ein Providerwechsel für mich nur noch im Notfall in Betracht.

Sehr gerne würde ich auch hier auf einen **nachhaltigen Provider** setzen, allerdings gibt es, außer mit [WEtell](https://www.wetell.de), bislang **keine geeigneten Alternativen**. Dieser nutzt das aus meiner Sicht nicht optimale **Vodafone-Netz**, weswegen er für mich bislang nicht in Frage kommt.

Da ich **kein Interesse an einem 24 Std. Bewegungsprofil** von mir habe, trage ich **seit Januar 2014** kein dauerhaft ins Mobilfunknetz eingebuchtes Handy mehr bei mir. Stattdessen befindet sich ein aktives Handy an einem der Standorte, welches ich per Messenger jederzeit **fernsteuern** kann. So werde ich über eingehende Anrufe / SMS **benachrichtigt** und kann entsprechend **reagieren**. Somit fallen dem Provider nicht mehr Daten in die Hände, als zwingend notwendig.

Sofern du mich auf einer meiner **Festnetz-Rufnummern** anrufst, wirst du von einer Cloud-Telefonanlage bei [**inopla / ComDesk**](https://www.inopla.de/) empfangen, wobei sämtliche Verbindungen von mir zu inopla / ComDesk **über eine SSL/TLS-veschlüsselte Verbindung** erfolgen. Das Unternehmen mit Sitz in Deutschland setzt nach eigenen Angaben ebenfalls auf Open Source-Software (Antwort auf meine Anfrage vom 13. Juni 2023):

> "Die meiste Software die wir verwenden ist Opensource. Das Hosting erfolgt auf eigenen Servern, in eigenen Racks, über eine eigene Internetverbindung in einem ISO 27001 zertifizierten Rechenzentrum.
>
>Berührungen mit externen Clouds wie AWS, Azure oder Google gibt es nur bei der Verwendung des Voicebots oder der Spracherkennung - für welche wir gesonderte Vertragsbedingungen haben.
>
> Sollten Sie noch weitere Informationen benötigen, welche nicht in dem Auftragsverarbeitungsvertrag zu finden sind, stehen wir Ihnen gerne zur Verfügung.

Am Liebsten sind mir allerdings **Ende-zu-Ende-verschlüsselte Gespräche** über **Telegram** oder **Signal**. Denn für mich spielt es keine Rolle, ob man etwas zu verheimlichen hat oder nicht. **Heimlich** Gespräche abhören oder mitschneiden **gefällt mir nicht**. Wenn jemand etwas wissen will, darf er / sie mich gerne fragen.

</details>

<br />

### Videokonferenzen
Tools und Dienstleister wie **Zoom**, **Microsoft Teams** oder andere nicht-quelloffene Software **nutze ich aus Überzeugung nicht**, da sie die Privatsphäre der Nutzer:innen nicht respektieren und einseitige Abhängigkeiten schaffen.

Stattdessen kommen bei mir **Open Source-Lösungen** wie [**BigBlueButton**](https://bigbluebutton.org/), [**Jitsi Meet**](https://jitsi.org/jitsi-meet/) oder [**MiroTalk**](https://sfu.mirotalk.com/) zum Einsatz.

<br />

<hr />

## Mobilität
- Ich fahre **ausschließlich gebrauchte Fahrzeuge**
- **Spritverbrauch** seit Anfang 2022 rund **4 Liter / 100km**
- **Öffentlichen Nahverkehr** nutze ich **nur bei Langstrecken**
- Ich **verzichte nicht explizit** auf Flugreisen

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Ich fahre schon immer **gebrauchte Fahrzeuge**, da hier die ökologische sowie die ökonomische Bilanz am ausgewogensten ausfällt. Ein Neufahrzeug kommt für mich – Stand heute – nicht in Frage. Egal wie verlockend die Konditionen für Unternehmer:innen auch klingen mögen – nein. Denn damit trage ich – ähnlich wie beim Mobilfunk – zu einem unnötig höheren Ressourcenverbrauch bei (Stichwort: Alle zwei Jahre ein neues Handy).

Mit dem aktuellen Fahrzeug (SEAT Ibiza) komme ich über meine gesamte Nutzungsdauer auf einen **Durchschnittsverbrauch** von **unter 5,5 Litern**, siehe [Verbrauchshistorie bei spritmonitor.de](https://www.spritmonitor.de/de/detailansicht/760793.html).

Seit Anfang 2022 fahre ich nur noch **rund 80-90 km/h**, da Verbrennungsmotoren in diesem Geschwindigkeitsbereich besonders **effizient** arbeiten. Dadurch hat sich mein **Durchschnittsverbrauch** nochmal **deutlich reduziert** und liegt nun bei rund **4 Litern / 100km**.

Auf Langstrecken (beispielsweise zwischen Hauptsitz und ehem. Niederlassung) komme ich sogar auf **3,6 Liter / 100km**. [Beleg1](belege/kfz/20230519_spritverbrauch_1.jpg) [Beleg 2](belege/kfz/20230519_spritverbrauch_2.jpg) [Beleg 3](belege/kfz/20230519_spritverbrauch_1.jpg)

**Öffentliche Verkehrsmittel** kommen für mich und meine Kund:innenbesuche in 95% der Fälle nicht als Alternative in Frage. Das liegt an der **unattraktiven Preisstruktur** und der **steigenden Unzuverlässigkeit** – gerade bei längeren Strecken. **Schade**, denn **unsere Nachbarländer** können das bereits **besser**.

Auf Flugreisen verzichte ich **nicht explizit**. Meine Flugreisen lassen sich bislang an **zwei Händen** abzählen:

Inlands-Flugreisen: **0**  
Auslands-Flugreisen: **7**

- 2005: DE - USA - DE
- 2010: DE - Spanien - DE
- 2013: DE - USA - DE
- 2016: DE - Spanien - DE
- 2017: DE - Italien - DE
- 2022: DE - Griechenland - DE
- 2023: DE - Portugal - DE

</details>

<br />

<hr />

## Pioniergeist
**Startups**, die **freie(re) Produkte** auf dem Markt etablieren wollen, unterstütze ich gerne finanziell durch einen Kauf.

Zu den von mir unterstützten **Hardwareprojekten** zählen bislang:
- [**Fairphone**](https://www.fairphone.com/de/) (Kickstarter-Kampagne 2013), ich war einer der ersten 10.185 Käufer:innen.
	- Das **erste** Smartphone, bei dem vier Komponenten aus **fairer Produktion** stammen

- [**SHIFT7+**](https://www.shiftphones.com/) (Startnext-Kampagne)
	- **Nachhaltiges / Faires** Tablet aus Deutschland

- [**PINEPHONE** & **PINETAB**](https://www.pine64.org/)
	- Vollständig auf **Linux** basierendes Smartphone / Tablet
	- Leider eher eine **Machbarkeitsstudie**, als alltagstaugliche Geräte

- [**NexDock 360**](https://nexdock.com)
	- Externes Display zur Nutzung mit USB-C und Mini-HDMI

- [**Popcorn Computers Pocket P.C.**](https://popcorncomputer.com/) (Crowdfunding 2019/2020)
	- Gerät wurde leider **nie** geliefert

- [**Purism Librem 5**](https://puri.sm/) (Crowdfunding-Kampagne 2017, geliefert 2022 [Evergreen-Batch])
	- Ein vollständig auf **Linux** basierendes Smartphone, was alltagstauglich sein sollte
	- Leider eher eine **Machbarkeitsstudie**, als ein alltagstaugliches Gerät

<br />

<hr />

## Persönlicher Einkaufskorb (Lebens- und Reinigungsmittel)
Auch wenn meine private Lebensgestaltung nur **indirekt** mein Unternehmen beeinflusst, spielt es dennoch eine Rolle, **welche Produkte ich regelmäßig einkaufe**. Daher gehe ich in diesem Punkt genauer darauf ein.

- So gut wie nur **regionale** bzw. **Bio- und Fairtrade-Produkte**
- **Konventionelle (Marken-)Produkte** kommen **selten bis nie** in den Einkaufskorb

<details>
<summary>➡️ <strong>Ich möchte mehr Details erfahren...</strong></summary>
<br />

Ich kaufe größtenteils auf **regionalen Märkten** und im **Bio-Markt** ein. Dabei greife ich grundsätzlich zu Produkten mit **Bio-Siegel** (EU-Bio, Bioland, Naturland, demeter) und achte auf **Fairtrade** und **regionale Produkte**.

Konventionelle Produkte kommen nur **in absoluten Ausnahmefällen** in den Einkaufskorb, Markenprodukte quasi nie.

Wieso ich das tue? Ganz einfach: Ich bin es mir wert, dass ich Lebensmittel auswähle, die im **Einklang mit der Natur**, anstatt auf ihre Kosten produziert wurden und die mir guttun. Schließlich möchte auch ich **für gute Leistung angemessen bezahlt werden**. Leider kommt gerade bei konventionellen Produkten – ebenso wie bei Bioware mit EU-Biosiegel – bei der Erzeuger:in nur **ein Bruchteil** dessen an, was ich als Verbraucher dafür bezahle. Dieses Verhältnis ist bei regionalen Märkten und im Bio-Markt **ausgewogener**.

**Kaffee** beziehe ich entweder von regionalen Röstereien oder bio-zertifiziert von [SONNENTOR](https://www.sonnentor.com/de-at/onlineshop/kaffee-kakao), [Lebensbaum](https://www.lebensbaum.com/de/produkte/kaffee) oder [Mount Hagen](https://www.mounthagen.de/bio-kaffees/).

Die **Bio-Milch- und Milch-Alternativprodukte** stammen von [Berchtesgadener Land](https://bergbauernmilch.de/de/unsere-produkte.html?linie=bioalpenmilch), [Voelkel](https://voelkeljuice.de/produktart/pflanzendrinks/), [Allos](https://www.allos.de/produkte/pflanzliche-drinks/), [Natumi](https://www.natumi.com/produkte/), und Weiteren.

**Toilettenpapier und Hygienepapiere** stammen von der Firma [DANKE](https://www.danke.de/produkte/toilettenpapier/) bzw. von [MACH MIT](https://wepa-mach-mit.de/pure/).

Bei **Reinigungsmitteln** setze ich ebenfalls auf **natürliche Inhaltsstoffe** und **Bio-Produkte**, beispielsweise von [Frosch](https://frosch.de/de/), [Almawin](https://www.almawin.de), [Claro](https://www.claro.at), [Ecover](https://www.ecover.com/de/) oder [lavera](https://www.lavera.de).

Auf konventionelle (Marken-)Produkte **verzichte ich** in diesem Bereich völlig.

</details>

<br />

### CO2-Einsparung
- Ich sehe die wahren Hebel für die zwingend notwendige Veränderung **nicht** in CO2-Rechnern oder dem CO2-Zertifikatshandel. Daher gibt es in diesem Bericht keine Informationen dazu.

<br />

<details>
<summary>➡️ <strong>Ich möchte mehr dazu erfahren...</strong></summary>
<br />

Persönlich halte ich nichts von CO2-Rechnern und Maßnahmen wie dem CO2-Zertifikatshandel. Dieser Marketing-Trick, [den der Energiekonzern BP 2004 ins Leben gerufen hat](https://de.wikipedia.org/wiki/CO2-Bilanz#Aussagekraft), gaukelt uns vor, dass jede:r Einzelne durch derartige Kompensation maßgeblich etwas zur "Verhinderung" der Klimakrise beitragen müsste bzw. könnte. Dieses Narrativ **unterstütze ich nicht**. Daher wird es in diesem Bericht kein Wort zu CO2-Emissionen und auch keine weiteren Informationen zu "meinem CO2-Fußabdruck" geben.

In den folgenden Maßnahmen sehe ich den **wahren Hebel** für die in unserer Gesellschaft **notwendigen Veränderungen**, um die zwangsläufig eintretende globale (Klima)-Krise zu "gestalten", weswegen ich mich **ausschließlich darauf fokussiere**:

**Zwischenmenschlich**
- Bewusstsein schaffen
- Achtsamkeit
- Liebe (vor allem bei dem und für das, was wir tagtäglich tun)
- Ehrlichkeit und Transparenz
- Einander zuhören und verstehen, statt nur verstanden werden wollen

**Konkrete Maßnahmen**
- Aufforstung der Wälder
- Trockengelegte Moore wieder vernässen
- Förderung von Artenvielfalt und unterschiedlichen Lebensräumen
- Regional denken und handeln (statt global)
- Bioprodukte statt Massenproduktion
- Finanzielle Unterstützung von Initiativen, die sich für oben genannte Punkte einsetzen

</details>

<br />